var artist: String = "Mambo Dhuterere"
var numberOfDownloads: Long = 788_889L
var listOfTrackNames = listOf("Dare guru", "Zodzo", "Mundisesekedze", "Ahuwerere", "\'Hey Hey Hey\'")

fun main(){
    println(artist())
    println(numberOfDownloads())
    println(listOfTrackNames())
}

fun artist(): String{
    return artist
}

fun numberOfDownloads(): Long{
    return numberOfDownloads
}

fun listOfTrackNames(): List<String>{
    return listOfTrackNames
}
